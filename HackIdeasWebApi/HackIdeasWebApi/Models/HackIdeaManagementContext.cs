﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace HackIdeasWebApi.Models
{
    public partial class HackIdeaManagementContext : DbContext
    {
        public HackIdeaManagementContext()
        {
        }

        public HackIdeaManagementContext(DbContextOptions<HackIdeaManagementContext> options)
            : base(options)
        {
        }

        public virtual DbSet<HackEmployee> HackEmployee { get; set; }
        public virtual DbSet<HackIdea> HackIdea { get; set; }
        public virtual DbSet<HackTag> HackTag { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\SQLExpress;Initial Catalog=HackIdeaManagement;trusted_connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HackEmployee>(entity =>
            {
                entity.HasKey(e => e.EmpId)
                    .HasName("PK__Hack_Emp__AF2DBA79E94D44BB");

                entity.ToTable("Hack_Employee");

                entity.Property(e => e.EmpId).HasColumnName("EmpID");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HackIdea>(entity =>
            {
                entity.HasKey(e => e.IdeaId)
                    .HasName("PK__Hack_Ide__FE2182232724E8CA");

                entity.ToTable("Hack_Idea");

                entity.Property(e => e.IdeaId).HasColumnName("IdeaID");

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EmpId).HasColumnName("EmpID");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MadifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TagId).HasColumnName("TagID");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.HackIdea)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Hack_Idea__EmpID__29572725");

                entity.HasOne(d => d.Tag)
                    .WithMany(p => p.HackIdea)
                    .HasForeignKey(d => d.TagId)
                    .HasConstraintName("FK__Hack_Idea__TagID__2A4B4B5E");
            });

            modelBuilder.Entity<HackTag>(entity =>
            {
                entity.HasKey(e => e.TagId)
                    .HasName("PK__Hack_tag__657CFA4CABF36488");

                entity.ToTable("Hack_tag");

                entity.Property(e => e.TagId).HasColumnName("TagID");

                entity.Property(e => e.TagName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TagdDesc)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
