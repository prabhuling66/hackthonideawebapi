﻿using System;

namespace HackIdeasWebApi.Models
{
    public class HackIdeaViewModel
    {
        public int IdeaId { get; set; }
        public int EmpId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? TagId { get; set; }
        public int? UpVote { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? MadifiedDate { get; set; }
        public string TagName { get; set; }
    }
}
