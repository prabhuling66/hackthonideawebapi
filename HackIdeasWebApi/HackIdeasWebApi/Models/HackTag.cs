﻿using System;
using System.Collections.Generic;

namespace HackIdeasWebApi.Models
{
    public partial class HackTag
    {
        public HackTag()
        {
            HackIdea = new HashSet<HackIdea>();
        }

        public int TagId { get; set; }
        public string TagName { get; set; }
        public string TagdDesc { get; set; }

        public virtual ICollection<HackIdea> HackIdea { get; set; }
    }
}
