﻿using System;
using System.Collections.Generic;

namespace HackIdeasWebApi.Models
{
    public partial class HackIdea
    {
        public int IdeaId { get; set; }
        public int EmpId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? TagId { get; set; }
        public int? UpVote { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? MadifiedDate { get; set; }
        public virtual HackEmployee Emp { get; set; }
        public virtual HackTag Tag { get; set; }
    }
}
