﻿using System;
using System.Collections.Generic;

namespace HackIdeasWebApi.Models
{
    public partial class HackEmployee
    {
        public HackEmployee()
        {
            HackIdea = new HashSet<HackIdea>();
        }

        public int EmpId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool? IsActive { get; set; }
        

        public virtual ICollection<HackIdea> HackIdea { get; set; }
    }
}
