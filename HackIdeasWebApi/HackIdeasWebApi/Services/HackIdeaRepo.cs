﻿using HackIdeasWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HackIdeasWebApi.Services
{
    public class HackIdeaRepo : IHackIdeaRepo
    {
        private readonly HackIdeaManagementContext _context;
        public HackIdeaRepo(HackIdeaManagementContext context)
        {
            _context = context;
        }
        public int AddHackIdea(HackIdea hackIdea)
        {
            try
            {
                if (hackIdea.IdeaId > 0)
                {
                    hackIdea.MadifiedDate = DateTime.Now;
                    _context.HackIdea.Update(hackIdea);
                    return _context.SaveChanges();
                }
                else
                {
                    // hackIdea.EmpId = 1;
                    hackIdea.UpVote = 0;
                    hackIdea.CreationDate = DateTime.Now;
                    hackIdea.IsActive = true;
                    _context.HackIdea.Add(hackIdea);
                    return _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HackIdeaViewModel> GetAllIdea()
        {
            try
            {
                var res = _context.HackIdea.Where(x => x.IsActive == true).Select(idea => new HackIdeaViewModel()
                {
                    EmpId = idea.EmpId,
                    IdeaId = idea.IdeaId,
                    Title = idea.Title,
                    Description = idea.Description,
                    TagId = idea.TagId,
                    UpVote = idea.UpVote,
                    IsActive = idea.IsActive,
                    CreationDate = idea.CreationDate,
                    MadifiedDate = idea.MadifiedDate,
                    TagName = _context.HackTag.Where(tag => tag.TagId == idea.TagId).Select(tag => tag.TagName).FirstOrDefault()
                }).ToList();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HackIdeaViewModel GetIdeaById(int id)
        {
            try
            {
                var res = _context.HackIdea.Where(idea => idea.IdeaId == id).Select(idea => new HackIdeaViewModel()
                {
                    EmpId = idea.EmpId,
                    IdeaId = idea.IdeaId,
                    Title = idea.Title,
                    Description = idea.Description,
                    TagId = idea.TagId,
                    UpVote = idea.UpVote,
                    IsActive = idea.IsActive,
                    CreationDate = idea.CreationDate,
                    MadifiedDate = idea.MadifiedDate,
                    TagName = _context.HackTag.Where(tag => tag.TagId == idea.TagId).Select(tag => tag.TagName).FirstOrDefault()
                }).FirstOrDefault();
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int UpdateHackIdea(HackIdea hackIdea)
        {
            try
            {
                _context.HackIdea.Update(hackIdea);
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteIdea(int Id)
        {
            var existingRecord = _context.HackIdea.Where(x => x.IdeaId == Id).FirstOrDefault();
            try
            {
                existingRecord.IsActive = false;
                existingRecord.MadifiedDate = DateTime.Now;
                return _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HackEmployee RegisterEmployee(HackEmployee employee)
        {
            try
            {
                _context.HackEmployee.Add(employee);
                var res = _context.SaveChanges();
                return employee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HackTag> GetAllTag()
        {
            try
            {
                return _context.HackTag.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HackEmployee UserExist(string userId, string password)
        {
            HackEmployee employee = new HackEmployee();
            try
            {
                employee = _context.HackEmployee.FirstOrDefault(x => x.UserId == userId && x.Password == password);
                return employee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HackEmployee GetUser(string userName)
        {
            HackEmployee employee = new HackEmployee();
            try
            {
                employee = _context.HackEmployee.FirstOrDefault(x => x.UserId == userName);
                return employee;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public HackIdea UpVote(HackIdea ideaModel)
        {
            try
            {
                //var record = _context.HackIdea.FirstOrDefault(x => x.IdeaId == ideaId);

                ideaModel.UpVote += 1;
                ideaModel.MadifiedDate = DateTime.Now;
                _context.HackIdea.Update(ideaModel);
                _context.SaveChanges();
                return ideaModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
