﻿using HackIdeasWebApi.Models;
using System.Collections.Generic;

namespace HackIdeasWebApi.Services
{
    public interface IHackIdeaRepo
    {
        int AddHackIdea(HackIdea hackIdea);
        int UpdateHackIdea(HackIdea hackIdea);
        int DeleteIdea(int Id);
        List<HackIdeaViewModel> GetAllIdea();
        HackIdeaViewModel GetIdeaById(int id);
        HackEmployee RegisterEmployee(HackEmployee employee);
        List<HackTag> GetAllTag();
        HackEmployee UserExist(string userId, string password);
        HackEmployee GetUser(string userName);
        HackIdea UpVote(HackIdea ideaModel);
    }
}
