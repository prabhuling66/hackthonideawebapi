﻿using HackIdeasWebApi.Models;
using HackIdeasWebApi.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace HackIdeasWebApi.Handlers
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IHackIdeaRepo _repository;
        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock, IHackIdeaRepo repo) : base(options, logger, encoder, clock)
        {
            _repository = repo;
        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if(!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Authorization was not found");
            try
            {
                var authenticationHeadrValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var bytes = Convert.FromBase64String(authenticationHeadrValue.Parameter);
                string[] credentials = Encoding.UTF8.GetString(bytes).Split(":");
                string UserID = credentials[0];
                string Password = credentials[1];
                HackEmployee employee = _repository.UserExist(UserID, Password);
                if (employee == null)
                    return AuthenticateResult.Fail("Invalid Username or password");
               
                else
                {
                    var claims = new[] { new Claim(ClaimTypes.Name, employee.UserId) };
                    var identity = new ClaimsIdentity(claims, Scheme.Name);
                    var princiapl = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(princiapl, Scheme.Name);
                    return AuthenticateResult.Success(ticket);
                }

            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail("Error has occured");
            }
            //throw new System.NotImplementedException();
        }
    }
}
