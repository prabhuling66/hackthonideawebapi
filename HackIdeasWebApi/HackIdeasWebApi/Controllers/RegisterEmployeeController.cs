﻿using HackIdeasWebApi.Models;
using HackIdeasWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HackIdeasWebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RegisterEmployeeController : Controller
    {
        private readonly IHackIdeaRepo _repository;
        public RegisterEmployeeController(IHackIdeaRepo repo)
        {
            _repository = repo;
        }
        [HttpPost]
        [Route("{action}")]
        [Route("api/RegisterEmployee/AddEmployee")]
        public HackEmployee AddEmployee(HackEmployee employee)
        {
            try
            {
                employee.IsActive = true;
                return _repository.RegisterEmployee(employee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
