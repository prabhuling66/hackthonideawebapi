﻿using HackIdeasWebApi.Models;
using HackIdeasWebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackIdeasWebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class HackIdeaController : ControllerBase
    {

        private readonly ILogger<HackIdeaController> _logger;
        private readonly IHackIdeaRepo _repository;
        public HackIdeaController(ILogger<HackIdeaController> logger, IHackIdeaRepo repo)
        {
            _logger = logger;
            _repository = repo;
        }

        [HttpGet]
        [Route("{action}")]
        [Route("api/HackIdea/GetAllIdea")]
        public IEnumerable<HackIdeaViewModel> GetAllIdea()
        {
            try
            {
                return _repository.GetAllIdea();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        [Route("{action}")]
        [Route("api/HackIdea/GetIdeaById")]
        public HackIdeaViewModel GetIdeaById(int id)
        {
            try
            {
                return _repository.GetIdeaById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        [HttpPost]
        [Route("{action}")]
        [Route("api/HackIdea/AddIdea")]
        public int AddIdea(HackIdea hackIdea)
        {
            try
            {
               return _repository.AddHackIdea(hackIdea);
            }
            catch(Exception ex)
            {
                throw ex;  
            }
        }

        [HttpPost]
        [Route("{action}")]
        [Route("api/HackIdea/AddEmployee")]
        public HackEmployee AddEmployee(HackEmployee employee)
        {
            try
            {
                employee.IsActive = true;
                return _repository.RegisterEmployee(employee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        [Route("{action}")]
        [Route("api/HackIdea/GetAllTag")]
        public List<HackTag> GetAllTag()
        {
            try
            {
                return _repository.GetAllTag();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpDelete]
        [Route("{action}")]
        [Route("api/HackIdea/DeleteIdea")]
        public int DeleteIdea(int ideaId)
        {
            try
            {
                return _repository.DeleteIdea(ideaId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpPut]
        [Route("{action}")]
        [Route("api/HackIdea/Upvote")]
        public HackIdea Upvote(HackIdea ideaModel)
        {
            try
            {
                return _repository.UpVote(ideaModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
