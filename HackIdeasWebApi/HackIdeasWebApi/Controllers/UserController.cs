﻿using HackIdeasWebApi.Models;
using HackIdeasWebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HackIdeasWebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IHackIdeaRepo _repository;
        public UserController(IHackIdeaRepo repo)
        {
            _repository = repo;
        }

        [HttpGet]
        [Route("{action}")]
        [Route("api/User/GetUser")]
        public HackEmployee GetUser()
        {
            string username = HttpContext.User.Identity.Name;
            var employee = _repository.GetUser(username);
            return employee;
        }
    }
}
