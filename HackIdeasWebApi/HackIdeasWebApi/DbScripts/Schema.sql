﻿Create Database HackIdeaManagement


Create Table Hack_Employee
(EmpID int identity(1,1) primary key,
 FirstName varchar(30),
 LastName  varchar(30),
 UserID    varchar(30),
 [Password]  varchar(30),
 Email varchar(50),
 Mobile varchar(12),
 IsActive bit)


 Create Table Hack_tag
( TagID int identity(1,1) primary key,
  TagName varchar(20),
  TagdDesc varchar(100))


  Create table Hack_Idea
( IdeaID int identity (1,1) primary key,
  EmpID int not null,
  Title varchar(100) not null,
  [Description] varchar(200),
  TagID int,
  UpVote int,
  IsActive bit not null default(1),
  CreationDate DateTime,
  MadifiedDate DateTime,
  FOREIGN KEY (EmpID) REFERENCES Hack_Employee(EmpID),
  FOREIGN KEY (TagID) REFERENCES Hack_Tag(TagID)
  )